#pragma once

#include "Destroyable.h"

class Fish : public Destroyable
{
public:
    virtual bool init();
    void update(float);
    void die();

    int getPoints();

    bool isAlive();
    bool burn();

    CREATE_FUNC(Fish);

protected:
    cocos2d::Texture2D skeleton;
    float speed;
    float acceleration = 0.0;
    float rotationSpeed;
    float dmg;
    cocos2d::Vec2 direction;
    cocos2d::Vec2 startDirection;
    cocos2d::Vec2 targetDirection;
    float percent();
};