#include "CookedFish.h"

USING_NS_CC;

bool CookedFish::init()
{
    int r = random(0, 2);
    std::string fileName = "";
    
    if (r == 0)
    {
        fileName = "Effects/Sushi.png";
    }
    else if (r == 1)
    {
        fileName = "Effects/Sushi.png";
    }
    else if (r == 2)
    {
        fileName = "Effects/Sushi.png";
    }

    if (!Sprite::initWithFile(fileName))
    {
        return false;
    }

    return true;
}

void CookedFish::update(float dt)
{
    if (numberOfRunningActions() == 0)
    {
        getParent()->removeChild(this);
    }
}