#include "SmallFish.h"

#define PI 3.14
#define MATH_RAD_TO_DEG 180/PI
#define MATH_DEG_TO_RAD PI/180

USING_NS_CC;

bool SmallFish::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();


    if (!initWithFile("Fish/SmallFish.png"))
    {
        return false;
    }

    auto image = new Image();
    image->initWithImageFile("Fish/SmallFishSkeleton.png");
    skeleton.initWithImage(image);

    startDirection = Vec2(0, 1);
    targetDirection = Vec2(0, 1);
    float angle = -PI / 24 + (rand() % 1000) / 1000.0 * PI / 12;

    targetDirection = targetDirection.rotateByAngle(Vec2(0, 0), angle);
    int r = rand() % 10000;

    setScaleX(visibleSize.width / 1080);
    setScaleY((-1 + 2 * std::signbit(angle)) * visibleSize.width / 1080);


    speed = (130.0f + rand() % 25) * visibleSize.width / 1080;
    dmg = 25.0f;
    points = 1;

    scheduleUpdate();

    return true;
}


void SmallFish::setPositionAround(Vec2 center)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    int radius = 100;

    radius *= visibleSize.width / 1080;

    int x = -radius + rand() % (2 * radius);
    int y = -radius + rand() % (2 * radius);

    this->setPosition(Vec2(center.x + x, center.y + y));

}
