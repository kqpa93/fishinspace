#include "Spawner.h"
#include "MainLayer.h"
#include <stdlib.h>

#define RAND_MIN 100
#define PI 3.14

USING_NS_CC;

bool Spawner::init()
{
    if (!Node::init())
    {
        return false;
    }
    
    fishTimer = 0.0f;
    shuttleTimer = 0.0f;
    shuttleDeltaTime = 1.1f;
    fishDeltaTime = 1.2f;
    fishRandomMax = 100;
    shuttleRandomMax = 100;

    return true;
}

void Spawner::update(float dt)
{

    fishTimer += dt;
    shuttleTimer += dt;
    if (fishTimer > fishDeltaTime)
    {
        auto visibleSize = Director::getInstance()->getVisibleSize();
        auto scene = dynamic_cast<MainLayer*>(getParent());
        Destroyable * f = nullptr;
        int nr = 0;

        if (fishRandomMax > RAND_MIN)
        {
            --fishRandomMax;
        }

        auto r = random(0, fishRandomMax);

        fishTimer = 0.0f;

        fishDeltaTime -= 0.001f;
        CCLOG("%f", fishDeltaTime);
        
        if (r <= 100)
        {
            if (r >= 90)
            {
                f = SuperFish::create();
            }
            else if (r >= 60)
            {
                nr = 2 + rand() % 2;
            }
            else
            {
                f = Fish::create();
            }

        }
        if (f != nullptr)
        {
            scene->addChild(f);
            scene->addDestroyable(f);
        }
        
        for (int i = 0; i < nr; ++i)
        {
            auto sf = SmallFish::create();
            int x = 0.2*visibleSize.width + (rand() % (int)(0.6*visibleSize.width));
            sf->setPositionAround(Vec2(x, -300));
            scene->addChild(sf);
            scene->addDestroyable(sf);
        }
    }

    if (shuttleTimer > shuttleDeltaTime)
    {
        auto visibleSize = Director::getInstance()->getVisibleSize();
        auto scene = dynamic_cast<MainLayer*>(getParent());
        Destroyable * f = nullptr;
        int nr = 0;

        if (shuttleRandomMax > RAND_MIN)
        {
            --shuttleRandomMax;
        }

        auto r = random(0, shuttleRandomMax);

        shuttleTimer = 0.0f;

        shuttleDeltaTime -= 0.001f;
        CCLOG("%f", shuttleDeltaTime);

        if (r <= 100)
        {
            if (r >= 40)
            {
                f = Plane::create();
            }
            else if (r >= 30)
            {
                f = Rocket::create();
            }
            else if (r >= 0)
            {
                f = Submarine::create();
            }
        }
        if (f != nullptr)
        {
            scene->addChild(f);
            scene->addDestroyable(f);
        }

        for (int i = 0; i < nr; ++i)
        {
            auto sf = SmallFish::create();
            int x = 0.2*visibleSize.width + (rand() % (int)(0.6*visibleSize.width));
            sf->setPositionAround(Vec2(x, -300));
            scene->addChild(sf);
            scene->addDestroyable(sf);
        }
    }

}

float Spawner::probability(float t, float speed, float max)
{
    return t/1200.0f;
}

void Spawner::turnOn()
{
	scheduleUpdate();
}

void Spawner::turnOff()
{
	fishTimer = 0.0f;
	shuttleTimer = 0.0f;
	unscheduleUpdate();
}