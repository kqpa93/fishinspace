#pragma once

#include "cocos2d.h"

class CookedFish : public cocos2d::Sprite
{
public:
    bool init();
    void update(float);

    CREATE_FUNC(CookedFish);
};

