#include "SuperFish.h"

#define PI 3.14
#define MATH_RAD_TO_DEG 180/PI
#define MATH_DEG_TO_RAD PI/180

USING_NS_CC;

bool SuperFish::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();


    if (!initWithFile("Fish/BigFish.png"))
    {
        return false;
    }

    auto image = new Image();
    image->initWithImageFile("Fish/BigFishSkeleton.png");
    skeleton.initWithImage(image);

    startDirection = Vec2(0, 1);
    targetDirection = Vec2(0, 1);
    float angle = -PI / 24 + (rand() % 1000) / 1000.0 * PI / 12;

    targetDirection = targetDirection.rotateByAngle(Vec2(0, 0), angle);
    int r = rand() % 10000;

    if (angle < 0.0f)
    {
        this->setPosition(0.2*visibleSize.width + 0.5*visibleSize.width*r / 10000.0, -300);
    }
    else
    {
        this->setPosition(0.3*visibleSize.width + 0.5*visibleSize.width*r / 10000.0, -300);
    }

    setScaleX(visibleSize.width / 1080);
    setScaleY((-1 + 2 * std::signbit(angle)) * visibleSize.width / 1080);


    speed = (150.0f + rand() % 25) * visibleSize.width / 1080;
    dmg = 25.0f;
    points = 25;

    scheduleUpdate();
    
    return true;
}
