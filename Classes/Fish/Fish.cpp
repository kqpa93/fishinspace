#include "Fish.h"
#include "Scenes\MainScene.h"
#include "CookedFish.h"

#define PI 3.14
#define MATH_RAD_TO_DEG 180/PI
#define MATH_DEG_TO_RAD PI/180

USING_NS_CC;

bool Fish::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();


    if (!initWithFile("Fish/Fish.png"))
    {
        return false;
    }
    auto image = new Image();
    image->initWithImageFile("Fish/FishSkeleton.png");
    skeleton.initWithImage(image);

    startDirection = Vec2(0, 1);
    targetDirection = Vec2(0, 1);
    float angle = -PI / 24 + (std::rand() % 1000) / 1000.0 * PI / 12;

    targetDirection = targetDirection.rotateByAngle(Vec2(0, 0), angle);
    int r = std::rand() % 10000;

    if (angle < 0.0f)
    {
        this->setPosition(0.1 * visibleSize.width + 0.8 * visibleSize.width*r / 10000.0, -50);
    }
    else
    {
        this->setPosition(0.1 * visibleSize.width + 0.8 * visibleSize.width*r / 10000.0, -50);
    }

    setAnchorPoint(Point(0.5, 0.5));

    setScaleX(visibleSize.width / 1080);
    setScaleY((-1 + 2 * std::signbit(angle)) * visibleSize.width / 1080); 

    speed = (175 + std::rand() % 30) * visibleSize.width / 1080;
    dmg = 50.0f;
    points = 5;

    scheduleUpdate();

    return true;
}


void Fish::update(float dt)
{
    speed += acceleration*dt;

    auto scene = dynamic_cast<MainScene*>(getParent()->getParent());

    auto visibleSize = Director::getInstance()->getVisibleSize();

    direction = startDirection.lerp(targetDirection, percent());

    this->setRotation(atan2(direction.x,
                            direction.y) * MATH_RAD_TO_DEG - 90);
    this->setPosition(this->getPosition() + speed*direction*dt);

    auto layer = dynamic_cast<MainLayer*>(getParent());

    if (this->getPosition().y > visibleSize.height - getContentSize().height / 2)
    {

        if (alive && scene != nullptr && scene->gameLayer->runing)
        {
            scene->endgame();
            scene->gameLayer->runing = false;
						scene->gameLayer->clearDestroyables();
        }
        if (this->getPosition().y > visibleSize.height + getContentSize().height / 2)
        {
            layer->removeDestroyable(this);
            layer->removeChild(this);
            return;
        }
    }
    else if (this->getPosition().y < -400)
    {
        layer->removeDestroyable(this);
        layer->removeChild(this);
        return;
    }

    if (getScaleX() == 0)
    {
        for (int i = 0; i < points; ++i)
        {
            auto food = CookedFish::create();
            float _y = random(-this->getContentSize().width / 4, this->getContentSize().width / 4);
            float _x = random(-this->getContentSize().height / 4, this->getContentSize().height / 4);
        
            food->setPosition(getPosition() + Vec2(_x, _y));

            food->setScale(0.2 * visibleSize.width / 1080);

            auto move = MoveTo::create(0.5f, Vec2(20, visibleSize.height - 20));
            food->runAction(move);

            auto scale = ScaleTo::create(0.5f, 0);
            food->runAction(scale);

            int rotation = random(-180, 180);

            auto rotate = RotateBy::create(0.5f, rotation);
            food->runAction(rotate);

            food->scheduleUpdate();
            getParent()->addChild(food);
        }
        getParent()->removeChild(this);
        return;
    }
}

float Fish::percent()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    return this->getPositionY() / visibleSize.height;
}

int Fish::getPoints()
{
    return points;
}

bool Fish::isAlive()
{
    return alive;
}

bool Fish::burn()
{  
    alive = false;
    acceleration = -150;
    setTexture(&skeleton);

    return !alive;
}

void Fish::die()
{
    alive = false;
    
    int rotation = random(-180, 180);

    auto rotate = RotateBy::create(0.2f, rotation);
    runAction(rotate);

    auto scale = ScaleTo::create(0.2f, 0.0f);
    runAction(scale);
}