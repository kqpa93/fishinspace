#pragma once
#include "cocos2d.h"
#include "SmallFish.h"
#include "Fish.h"
#include "SuperFish.h"
#include "SpaceShips\Plane.h"
#include "SpaceShips\Satelite.h"
#include "SpaceShips\Rocket.h"
#include "SpaceShips\Submarine.h"
#include <stdlib.h>

class Spawner : public cocos2d::Node
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(Spawner);
		void turnOn();
		void turnOff();
private:
    float probability(float time, float speed, float max);
    float fishDeltaTime;
    float shuttleDeltaTime;
    float fishTimer;
    float shuttleTimer;
    int fishRandomMax;
    int shuttleRandomMax;
    int spawnCount;
    float spawnDeltaTime;
};