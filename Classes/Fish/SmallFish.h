#pragma once

#include "Fish.h"

class SmallFish : public Fish
{
public:
    bool init();
    CREATE_FUNC(SmallFish);
    void setPositionAround(cocos2d::Vec2 center);
};