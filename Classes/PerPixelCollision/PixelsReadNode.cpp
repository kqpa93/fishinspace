#include "PixelsReadNode.h"

USING_NS_CC;

PixelsReadNode * PixelsReadNode::instance = NULL;

PixelsReadNode * PixelsReadNode::getInstance()
{
    if (instance == NULL)
    {
        instance = new PixelsReadNode();
    }
    return instance;
}

void PixelsReadNode::setBuffer(unsigned char * b)
{
    buffer = b;
}

void PixelsReadNode::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
    _readPixelsCommand.init(_globalZOrder);
    _readPixelsCommand.func = CC_CALLBACK_0(PixelsReadNode::onDraw, this, transform, flags);
    renderer->addCommand(&_readPixelsCommand);}

void PixelsReadNode::onDraw(const Mat4& transform, uint32_t flags)
{
    kmGLPushMatrix();
    kmGLLoadMatrix(&transform);
    glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    DrawPrimitives::setDrawColor4F(0.0f, 1.0f, 0.0f, 1.0f);
    DrawPrimitives::drawRect(Vec2(x, y), Vec2(x+w, y+h));
    kmGLPopMatrix();
}

void PixelsReadNode::setDim(int x, int y, int w, int h)
{ 
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
}