#pragma once
#include "cocos2d.h"
class PixelsReadNode : public cocos2d::Node
{
public:
    void setDim(int x, int y, int w, int h);
    static PixelsReadNode * getInstance();
    void setBuffer(unsigned char * b);
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags);
protected:
    void onDraw(const cocos2d::Mat4& transform, uint32_t flags);
    cocos2d::CustomCommand _readPixelsCommand;
    static PixelsReadNode * instance;
    unsigned char * buffer;
    int x, y, w, h;
};

