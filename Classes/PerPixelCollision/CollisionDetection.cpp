#include "CollisionDetection.h"

#include "PixelsReadNode.h"

USING_NS_CC;
int CollisionDetection::count = 0;

CollisionDetection * CollisionDetection::instance = NULL;

CollisionDetection * CollisionDetection::getInstance()
{
    if (instance == NULL)
    {
        instance = new CollisionDetection();
    }

    return instance;
}

CollisionDetection::CollisionDetection()
{
    glProgram = new cocos2d::GLProgram();
    const GLchar * fragmentShader = String::createWithContentsOfFile("SolidColorShader.fsh")->getCString();
    glProgram->retain();
    glProgram->initWithByteArrays(ccPositionTextureColor_noMVP_vert, fragmentShader);
    glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
    glProgram->link();
    glProgram->updateUniforms();
    glProgram->use();

    //uniformColorRed = glGetUniformLocation(glProgram->getProgram(), "u_color_red");
    //uniformColorBlue = glGetUniformLocation(glProgram->getProgram(), "u_color_blue");
    buffer = (unsigned char *)malloc(sizeof(unsigned char) * 405 * 720 * 4);
}

bool CollisionDetection::isSpriteColliding(Sprite * spr, Triangle * triangle, RenderTexture * _rt)
{
    bool isColliding = false;

    Rect intersection;
    auto pixelsReader = PixelsReadNode::getInstance();

    Rect r1 = spr->boundingBox();
    Rect r2 = triangle->getBoundingBox();

    int pointsNr = 0;

    if (triangle->isPointInside(Vec2(r1.getMinX(), r1.getMinY())))
    {
        ++pointsNr;
    }
    if (triangle->isPointInside(Vec2(r1.getMaxX(), r1.getMinY())))
    {
        ++pointsNr;
    }
    if (triangle->isPointInside(Vec2(r1.getMaxX(), r1.getMaxY())))
    {
        ++pointsNr;
    }
    if (triangle->isPointInside(Vec2(r1.getMinX(), r1.getMaxY())))
    {
        ++pointsNr;
    }

    if (r1.containsPoint(*triangle->points[0]) &&
        r1.containsPoint(*triangle->points[1]) &&
        r1.containsPoint(*triangle->points[2]))
    {
        return true;
    }

    if (pointsNr == 4 )
    {
        return false;
    }

    if (triangle->intersectRect(r1))
    {

        int x, y, w, h;

        intersection = triangle->intersectionRect(r1);

        x = intersection.getMinX();
        y = intersection.getMinY();
        w = intersection.getMaxX() - x;
        h = intersection.getMaxY() - y;

        int numPixels = w*h * 4;

        CCLOG("R1: %f, %f, %f, %f", r1.getMinX(), r1.getMinY(), r1.getMaxX(), r1.getMaxY());
        CCLOG("R2: %f, %f, %f, %f", r2.getMinX(), r2.getMinY(), r2.getMaxX(), r2.getMaxY());

        CCLOG("Intersection calculated %d,%d,%d,%d,%d", x, y, w, h, numPixels);
        pixelsReader->setBuffer(buffer);
        pixelsReader->setDim(x, y, w, h);

        //buffer = (unsigned char *)realloc(buffer, sizeof(unsigned char)*numPixels);

        for (unsigned int i = 0; i < numPixels; ++i)
        {
            buffer[i] = 0;
        }

        spr->setGLProgram(glProgram);
        glProgram->use();

        _rt->beginWithClear(0, 0, 0, 0);

        spr->setBlendFunc(BlendFunc{ GL_SRC_ALPHA, GL_ONE });
        spr->visit();
        spr->setBlendFunc(BlendFunc{ CC_BLEND_SRC, CC_BLEND_DST });

        triangle->visit();

        pixelsReader->visit();

        _rt->end();

        spr->setGLProgram(GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, ccPositionTextureColor_noMVP_frag));

        Director::getInstance()->getRenderer()->render();
#if 0//(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
        std::string str = String::createWithFormat("rt%d.png", CollisionDetection::count)->getCString();

        if (_rt->saveToFile(str))
        {
            CCLOG("ZAPISANO");
            CollisionDetection::count++;
        }
#endif
        unsigned int step = 2;
        for (unsigned int i = 0; i < numPixels; i += step * 4)
        {
            // Here we check if a single pixel has both RED and BLUE pixels
            if (buffer[i] > 0 && buffer[i + 2] > 0)
            {
                //CCLOG("r:%d g:%d b:%d a:%d", buffer[i], buffer[i + 1], buffer[i + 2], buffer[i + 3]);
                isColliding = true;
                break;
            }
        }
    }
    
    return isColliding;
}