#pragma once

#include "cocos2d.h"
#include "./../Utilities/Triangle.h"

class CollisionDetection
{
public:
    static CollisionDetection* getInstance();
    bool isSpriteColliding(cocos2d::Sprite * spr, Triangle * triangle, cocos2d::RenderTexture * _rt);
private:
    static CollisionDetection* instance;
    static int count;
    CollisionDetection();

    cocos2d::GLProgram * glProgram;
    unsigned char * buffer;
    int uniformColorRed;
    int uniformColorBlue;
};

