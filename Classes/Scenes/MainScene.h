#pragma once
#include "cocos2d.h"
#include "MainLayer.h"
#include "Destroyable.h"
#include "Defines.h"
#include <stdlib.h>

class MainScene : public cocos2d::Scene
{
public:
	virtual bool init();
	void addPoints(int points);
	void update(float);
	CREATE_FUNC(MainScene);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);

	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	void reset(cocos2d::Ref*);
	void startGame(cocos2d::Ref*);
	void exit(cocos2d::Ref*);
	void endgame();

	GameState getGameState();
	void setGameState(GameState gameState);

	MainLayer*  gameLayer;
private:
	void initEndGameScreen();

	int score{ 0 };
	cocos2d::Layer * popupLayer;
	cocos2d::Layer * scoreLayer;
	cocos2d::Layer * menuLayer;
	cocos2d::LabelTTF* scoreLabel;
	cocos2d::LabelTTF* comboLabel;
	cocos2d::LabelTTF* multiplierLabel;
	cocos2d::LabelTTF* highScoreLabel;
	cocos2d::Label * label;

	bool runing{ true };
	GameState gameState{ GameState::MENU };
};