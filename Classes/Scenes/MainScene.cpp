#include "MainScene.h"
#include "MainLayer.h"
#include "Explosion.h"
#include "Defines.h"
#include <stdlib.h>

USING_NS_CC;

bool MainScene::init()
{
	if (!Scene::init())
	{
		return false;
	}

	//#HACK - loading explosion texture - too big to load on run
	auto e = Explosion::create();
	//END OF HACK

	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto keyboarListener = EventListenerKeyboard::create();

	keyboarListener->onKeyPressed = CC_CALLBACK_2(MainScene::onKeyPressed, this);
	keyboarListener->onKeyReleased = CC_CALLBACK_2(MainScene::onKeyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboarListener, this);

	gameLayer = MainLayer::create();
	popupLayer = Layer::create();
	scoreLayer = Layer::create();
	menuLayer = Layer::create();
	this->addChild(gameLayer, 0);
	this->addChild(popupLayer, 1000);
	this->addChild(menuLayer, 1000);
	this->addChild(scoreLayer, 100000);

	popupLayer->setPosition(0, visibleSize.height);
	scoreLayer->setPosition(0, visibleSize.height);

	auto playButton = MenuItemFont::create("Play", CC_CALLBACK_1(MainScene::startGame, this));
	playButton->setName(FONT_PATH);
	playButton->setColor(Color3B(220, 220, 220));
	playButton->setFontSize(96 * visibleSize.width / 1080);
	playButton->setPosition(0, 96 * visibleSize.width / 1080);


	auto buttonsMenu = Menu::create(playButton, NULL);
	buttonsMenu->setPosition(visibleSize / 2);
	menuLayer->addChild(buttonsMenu, 10001);

	scoreLabel = LabelTTF::create("Score: 0", FONT_PATH, 96 * visibleSize.width / 1080);
	scoreLabel->setColor(Color3B(80, 80, 80));
	scoreLabel->setPosition(20, visibleSize.height - 20);
	scoreLabel->setAnchorPoint(Point(0, 1));
	popupLayer->addChild(scoreLabel, 100000);

	multiplierLabel = LabelTTF::create("", FONT_PATH, 96 * visibleSize.width / 1080);
	multiplierLabel->setColor(Color3B(80, 80, 80));
	multiplierLabel->setPosition(visibleSize.width - 20, visibleSize.height - 20);
	multiplierLabel->setAnchorPoint(Point(1, 1));
	popupLayer->addChild(multiplierLabel, 100000);

	comboLabel = LabelTTF::create("Combo: 0", FONT_PATH, 96 * visibleSize.width / 1080);
	comboLabel->setColor(Color3B(80, 80, 80));
	comboLabel->setPosition(20, visibleSize.height - 40 - 64 * visibleSize.width / 1080);
	comboLabel->setAnchorPoint(Point(0, 1));
	popupLayer->addChild(comboLabel, 100000);

	initEndGameScreen();

	scheduleUpdate();

	return true;
}

void MainScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		Director::getInstance()->end();
	}
}

void MainScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event * event)
{

}

void MainScene::addPoints(int points)
{
	score += points;
}

void MainScene::update(float dt)
{
	if (scoreLabel->getNumberOfRunningActions() == 0 && scoreLabel->getScale() != 1.0f)
	{
		auto scale = ScaleTo::create(0.1f, 1.0f);
		scoreLabel->runAction(scale);
	}

	if (comboLabel->getNumberOfRunningActions() == 0)
	{
		if (comboLabel->getScale() == 1.3f)
		{
			auto scale = ScaleTo::create(0.5f, 1.01f);
			comboLabel->runAction(scale);
		}
		else if (comboLabel->getScale() == 1.01f)
		{
			comboLabel->setColor(Color3B(80, 80, 80));
			auto scale = ScaleTo::create(0.0f, 1.0f);
			comboLabel->runAction(scale);
		}
	}

	std::string stringScore = String::createWithFormat("Score: %d", score)->getCString();
	if (stringScore != scoreLabel->getString())
	{
		auto scale = ScaleTo::create(0.2f, 1.2f);
		scoreLabel->runAction(scale);
		scoreLabel->setString(stringScore);
	}

	auto combo = gameLayer->triGun->getCombo();
	std::string stringCombo = String::createWithFormat("Combo: %d", combo)->getCString();
	if (comboLabel->getString()[7] != '0' &&  combo == 0)
	{
		auto scale = ScaleTo::create(0.1f, 1.3f);
		comboLabel->setColor(Color3B(200, 0, 0));
		comboLabel->runAction(scale);
	}

	if (stringCombo != comboLabel->getString())
	{
		comboLabel->setString(stringCombo);
	}

	std::string stringMultiplier = String::createWithFormat("x%d", gameLayer->triGun->getMultiplier())->getCString();

	if (stringMultiplier != multiplierLabel->getString())
	{
		if (gameLayer->triGun->getMultiplier() == 5)
		{
			multiplierLabel->setColor(Color3B(200, 0, 0));
		}
		else
		{
			multiplierLabel->setColor(Color3B(80, 80, 80));
		}
		multiplierLabel->setString(stringMultiplier);
	}
}

void MainScene::endgame()
{
	gameLayer->pause();

	int s = 0;

	s = UserDefault::getInstance()->getIntegerForKey("score");

	auto visibleSize = Director::getInstance()->getVisibleSize();

	if ((!s || s < score) && score != 0)
	{
		highScoreLabel = LabelTTF::create("New high score!!!", FONT_PATH, 120 * visibleSize.width / 1080);
		highScoreLabel->setVisible(true);
		UserDefault::getInstance()->setIntegerForKey("score", score);
	}
	else
	{
		highScoreLabel->setVisible(false);
	}

	auto str = String::createWithFormat("Your Score: %d", score);
	label = Label::createWithTTF(str->getCString(), FONT_PATH, 96 * visibleSize.width / 1080);

	score = 0;
	gameLayer->triGun->resetCombo();

	this->setGameState(GameState::ENDGAME);
}


void MainScene::initEndGameScreen()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto alpha = Sprite::create("alpha.png");
	alpha->setPosition(visibleSize / 2);
	alpha->setAnchorPoint(Point(0.5, 0.5));
	alpha->setScale(visibleSize.width / 1080);
	scoreLayer->addChild(alpha, 10000);


	auto playButton = MenuItemFont::create("Restart", CC_CALLBACK_1(MainScene::reset, this));
	playButton->setName(FONT_PATH);
	playButton->setColor(Color3B(220, 220, 220));
	playButton->setFontSize(96 * visibleSize.width / 1080);
	playButton->setPosition(0, 96 * visibleSize.width / 1080);


	auto buttonsMenu = Menu::create(playButton, NULL);
	buttonsMenu->setPosition(visibleSize / 2);
	scoreLayer->addChild(buttonsMenu, 10001);

	highScoreLabel = LabelTTF::create("New high score!!!", FONT_PATH, 120 * visibleSize.width / 1080);
	highScoreLabel->setColor(Color3B(200, 0, 0));
	highScoreLabel->setPosition(visibleSize.width / 2, 240 * visibleSize.width / 1080);
	scoreLayer->addChild(highScoreLabel, 10001);

	auto str = String::createWithFormat("Your Score: %d", score);
	label = Label::createWithTTF(str->getCString(), FONT_PATH, 96 * visibleSize.width / 1080);
	label->setColor(Color3B(220, 220, 220));
	label->setPosition(visibleSize.width / 2, 120 * visibleSize.width / 1080);
	scoreLayer->addChild(label, 10001);
}

void MainScene::reset(Ref * psender)
{
	setGameState(GameState::PLAYING);
}

void MainScene::startGame(Ref * psender)
{
	setGameState(GameState::PLAYING);
}

void MainScene::setGameState(GameState gameState)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto moveUp = MoveTo::create(0.5f, Vec2(0, visibleSize.height));
	auto moveDown = MoveTo::create(0.4f, Vec2(0, 0));


	if (this->gameState == GameState::PLAYING)
	{
		if (gameState == GameState::ENDGAME)
		{
			// Turn off spawners 
			gameLayer->getFishSpawner()->turnOff();
			// Hide GameGUI
			popupLayer->runAction(moveUp);
			// Update achievemnets

			// Show score
			scoreLayer->runAction(moveDown);
		}
	}
	else if (this->gameState == GameState::MENU)
	{
		if (gameState == GameState::PLAYING)
		{
			// Turn on spawners 
			gameLayer->getFishSpawner()->turnOn();
			//turn ontri gun
			gameLayer->runing = true;
			// Hide Menu
			menuLayer->runAction(moveUp);
			// Show GameGUI
			popupLayer->runAction(moveDown);
		}
	}
	else if (this->gameState == GameState::ENDGAME)
	{
		if (gameState == GameState::PLAYING)
		{
			// hide score
			scoreLayer->runAction(moveUp);
			// Turn on spawners
			gameLayer->getFishSpawner()->turnOn();
			//turn ontri gun
			gameLayer->runing = true;
			//Show Game GUI
			popupLayer->runAction(moveDown);
		}
		if (gameState == GameState::MENU)
		{
			// Hide Score
			// Show Menu
		}
	}

	this->gameState = gameState;
}