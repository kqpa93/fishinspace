#pragma once
#define FONT_PATH "fonts/hemigraphy-Regular.ttf"


enum class GameState { MENU, PLAYING, ENDGAME };