#include "TriangularGun.h"
#include "Fish\Fish.h"
#include "SpaceShips\Shuttle.h"
#include "Destroyable.h"
#include "MainLayer.h"
#include "Scenes\MainScene.h"
#include "PerPixelCollision\CollisionDetection.h"

#define PI 3.14
#define MATH_DEG_TO_RAD PI/180.0
#define MAXTIME 20

USING_NS_CC;

bool TriangularGun::init()
{
	if (!Node::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();

	_rt = RenderTexture::create(visibleSize.width, visibleSize.height);
	_rt->setPosition(ccp(visibleSize.width, visibleSize.height));
	_rt->retain();
	_rt->setVisible(false);

	timeout = 0.0f;

	tmpPoint = nullptr;
	points[0] = points[1] = points[2] = nullptr;
	p[0] = p[1] = p[2] = nullptr;

#if 0//(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	auto mouseListener = EventListenerMouse::create();
	mouseListener->onMouseDown = CC_CALLBACK_1(TriangularGun::onMouseDown, this);
	mouseListener->onMouseUp = CC_CALLBACK_1(TriangularGun::onMouseUp, this);
	mouseListener->onMouseMove = CC_CALLBACK_1(TriangularGun::onMouseMove, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);
#endif
	touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(TriangularGun::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(TriangularGun::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(TriangularGun::onTouchMoved, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	scheduleUpdate();

	return true;
}

float TriangularGun::area(cocos2d::Point p1, cocos2d::Point p2, cocos2d::Point p3)
{
	float a = p1.distance(p2);
	float b = p1.distance(p3);
	float c = p2.distance(p3);

	float s = (a + b + c) / 2;

	return sqrt(s*(s - a)*(s - b)*(s - c));
}

bool TriangularGun::pointInTriangle(cocos2d::Vec2 pt, cocos2d::Vec2 p1, cocos2d::Vec2 p2, cocos2d::Vec2 p3)
{
	Vec2 v0 = p3 - p1;
	Vec2 v1 = p2 - p1;
	Vec2 v2 = pt - p1;

	double d00 = v0.dot(v0);
	double d01 = v0.dot(v1);
	double d02 = v0.dot(v2);
	double d11 = v1.dot(v1);
	double d12 = v1.dot(v2);

	double invDenom = 1 / (d00 * d11 - d01 * d01);
	double u = (d11 * d02 - d01* d12) * invDenom;
	double v = (d00 * d12 - d01 * d02) * invDenom;
	bool check = (u >= 0) && (v >= 0) && (u + v < 1);
	return check;
}

void TriangularGun::onMouseMove(Event * event)
{
}

void TriangularGun::onMouseUp(Event * event)
{
	EventMouse *e = (EventMouse*)event;

	Vec2 position = e->getLocationInView();
	position.y += Director::getInstance()->getVisibleSize().height;
	//CCLOG("mouse x: %f, mouse y: %f", position.x, position.y);

	calculatePosition(position);
}

void TriangularGun::onMouseDown(Event * event)
{


}

void TriangularGun::attack()
{
	auto scene = dynamic_cast<MainScene*>(getParent()->getParent());

	Vector<Destroyable*> children = layer->getDestroyables();

	int pt = 0;
	int counter = 0;
	bool shuttleInWay = false;
	bool friedFish = false;

	auto visibleSize = Director::getInstance()->getVisibleSize();

	float scale = visibleSize.width / 1080;
	float length = 200.0f * scale;


	p[0] = new Vec2(*points[0] - (points[1]->getMidpoint(*points[2]) - *points[0]).getNormalized()*length);
	p[1] = new Vec2(*points[1] - (points[0]->getMidpoint(*points[2]) - *points[1]).getNormalized()*length);
	p[2] = new Vec2(*points[2] - (points[0]->getMidpoint(*points[1]) - *points[2]).getNormalized()*length);

	auto tri = new Triangle(*points[0], *points[1], *points[2]);
	auto bigTri = new Triangle(*p[0], *p[1], *p[2]);
	int nr = 0;
	int nrR = 0;
	if (!children.empty())
	{
		for (auto& child : children)
		{
			++nr;
			//auto spr = dynamic_cast<Sprite*>(destroyable);
			if (tri->getBoundingBox().intersectsRect(child->getBoundingBox()) && bigTri->isPointInside(child->getPosition()))
			{
				++nrR;

				if (CollisionDetection::getInstance()->isSpriteColliding(child, tri, _rt))
				{
					friedFish = child->burn();

					if (!friedFish)
					{
						if (scene != nullptr && scene->gameLayer->runing)
						{
							shuttleInWay = true;
							scene->endgame();
							scene->gameLayer->runing = false;
						}
					}
				}
				else if (tri->isPointInside(child->getPosition()))
				{
					if (child->isAlive())
					{
						int p = child->getPoints();
						if (p > 0)
						{
							++counter;
							pt += p;
							layer->removeDestroyable(child);
							((Fish*)child)->die();
						}
						else if (scene->gameLayer->runing)
						{
							child->burn();
							shuttleInWay = true;
							scene->endgame();
							scene->gameLayer->runing = false;
						}
					}
				}
			}
			CCLOG("w sumie: %d", nr);
			CCLOG("przetwarzamy: %d", nrR);
		}
	}
	if (!shuttleInWay && !friedFish)
	{
		combo += counter * counter;
	}
	else
	{
		combo = 0;
	}

	if (!shuttleInWay)
	{
		scene->addPoints(pt * getMultiplier());
	}
	ready = false;
	timeout = MAXTIME * area(*points[0], *points[1], *points[2]) / (Director::getInstance()->getVisibleSize().height * Director::getInstance()->getVisibleSize().width);
	points[0] = points[1] = points[2] = nullptr;
}

void TriangularGun::update(float dt)
{

}

float TriangularGun::getTimeout()
{
	return timeout;
}


bool TriangularGun::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event)
{
	if (!layer->runing)
	{
		return false;
	}

	Vec2 position = touch->getLocationInView();
	position.y = Director::getInstance()->getVisibleSize().height - position.y;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	position.clamp(Vec2(0, 0), (Vec2)visibleSize);

	tmpPoint = new Vec2(position);

	return true;
}

void TriangularGun::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event)
{
	if (!layer->runing)
	{
		return;
	}

	tmpPoint = nullptr;
	Vec2 position = touch->getLocationInView();
	position.y = Director::getInstance()->getVisibleSize().height - position.y;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	position.clamp(Vec2(0, 0), (Vec2)visibleSize);

	calculatePosition(position);
}

void TriangularGun::onTouchMoved(cocos2d::Touch* touch, ::Event* event)
{
	if (!layer->runing)
	{
		return;
	}

	Vec2 position = touch->getLocationInView();
	position.y = Director::getInstance()->getVisibleSize().height - position.y;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	position.clamp(Vec2(0, 0), (Vec2)visibleSize);

	tmpPoint = new Vec2(position);

}


void TriangularGun::draw(Renderer* renderer, const kmMat4& transform, uint32_t flags)
{
	_customCommand.init(_globalZOrder);
	_customCommand.func = CC_CALLBACK_0(TriangularGun::onDraw, this, transform, flags);
	renderer->addCommand(&_customCommand);

}

void TriangularGun::calculatePosition(Vec2 pos)
{
	if (runing)
	{
		if (points[0] == nullptr)
		{
			points[0] = new Vec2(pos);
		}
		else if (points[1] == nullptr)
		{
			points[1] = new Vec2(pos);
		}
		else if (points[2] == nullptr)
		{
			points[2] = new Vec2(pos);
			attack();
		}
	}
}

void TriangularGun::onDraw(const Mat4 &transform, uint32_t flags)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();

	kmGLPushMatrix();
	kmGLLoadMatrix(&transform);
	ccDrawColor4F(0.0f, 0.0f, 1.0f, 0.4f);
	Point p1[3];
	Point p2[3];
	if (points[0] != nullptr)
	{
		DrawPrimitives::drawSolidCircle(*points[0], 10 * visibleSize.width / 1080.0, 360, 10);

		if (points[1] != nullptr)
		{
			DrawPrimitives::drawSolidCircle(*points[1], 10 * visibleSize.width / 1080.0, 360, 10);

			if (tmpPoint != nullptr)
			{
				p1[0] = *points[0];
				p1[1] = *points[1];
				p1[2] = *tmpPoint;

				DrawPrimitives::drawSolidPoly(p1, 3, Color4F(0.0f, 0.0f, 1.0f, 0.4f));
			}
			else
			{
				ccDrawLine(*points[0], *points[1]);
			}
		}
		else if (tmpPoint != nullptr)
		{
			ccDrawLine(*points[0], *tmpPoint);
		}
	}

	if (tmpPoint != nullptr)
	{
		DrawPrimitives::drawSolidCircle(*tmpPoint, 10 * visibleSize.width / 1080.0, 360, 10);
	}

	kmGLPopMatrix();
}

int TriangularGun::getMultiplier()
{
	int m = 1;
	int p = 1;
	while (true)
	{
		if (combo > 5 * p && m < 5)
		{
			p += 2;
			++m;
		}
		else
		{
			break;
		}
	}

	return  m;
}

int TriangularGun::getCombo()
{
	return combo;
}

void TriangularGun::resetCombo()
{
	combo = 0;
}

bool TriangularGun::pixelPerfectCollision(Sprite * sprite, Vec2 p1, Vec2 p2, Vec2 p3)
{
#if 0
	auto glProgram = new CCGLProgram();
	glProgram->retain();
	glProgram->initWithVertexShaderFilename("SolidVertexShader.vsh", "SolidColorShader.fsh");
	glProgram->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
	glProgram->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);
	glProgram->link();
	glProgram->updateUniforms();
	glProgram->use();

	int uniformColorRed = glGetUniformLocation(glProgram->getProgram(), "u_color_red");
	int uniformColorBlue = glGetUniformLocation(glProgram->getProgram(), "u_color_blue");



	// A large buffer created and re-used again and again to store glReadPixels data
	auto buffer = (ccColor4B *)malloc(sizeof(ccColor4B) * 10000);

#endif // 0

	return false;
}

void TriangularGun::setLayer(MainLayer* mainLayer)
{
	layer = mainLayer;
}