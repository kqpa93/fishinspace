#pragma once
#include "cocos2d.h"
#include <vector>

class MainLayer;

class TriangularGun : public cocos2d::Node
{
public:
    bool init();
    CREATE_FUNC(TriangularGun);
    cocos2d::Vec2* points[3];
    cocos2d::Vec2* tmpPoint;

    void onMouseDown(cocos2d::Event * event);
    void onMouseUp(cocos2d::Event * event);
    void onMouseMove(cocos2d::Event * event);

    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void draw(cocos2d::Renderer* renderer, const cocos2d::kmMat4& transform, uint32_t flags);
    bool runing = true;
    void update(float);
    float getTimeout();
    cocos2d::EventListenerTouchOneByOne* touchListener;
		int getCombo();
		void resetCombo();
    int getMultiplier();
    bool pixelPerfectCollision(cocos2d::Sprite * sprite, cocos2d::Vec2 p1, cocos2d::Vec2 p2, cocos2d::Vec2 p3);
		void setLayer(MainLayer*);
protected:
    void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
    cocos2d::CustomCommand _customCommand;
    float area(cocos2d::Point p1, cocos2d::Point p2, cocos2d::Point p3);
    bool pointInTriangle(cocos2d::Vec2 pt, cocos2d::Vec2 p1, cocos2d::Vec2 p2, cocos2d::Vec2 p3);
    void attack();
    void calculatePosition(cocos2d::Vec2 pos);
    bool ready = true;
    float timeout;
    int combo = 0;
    cocos2d::Vec2 * p[3];
    cocos2d::RenderTexture * _rt;
		MainLayer * layer;
};