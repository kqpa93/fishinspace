#pragma once
#include "cocos2d.h"

class Cloud : public cocos2d::Sprite
{
public:
    bool init();
    void update(float);
    void setSpeed(int w, int y);
    CREATE_FUNC(Cloud);
private:
    int speed;
};

