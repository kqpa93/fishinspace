#pragma once
#include "cocos2d.h"

class Destroyable : public cocos2d::Sprite
{
public:
    virtual bool burn();
    virtual int getPoints();
    virtual bool isAlive();
protected:
    int points = 0;
    bool alive = true;
};

