#include "CloudSpawner.h"
#include "Cloud.h"
#define RAND_MAX 2
USING_NS_CC;

bool CloudSpawner::init()
{
    if (!Node::init())
    {
        return false;
    }
    timer = 0.0f;
    deltaTime = 5.0f;
    wind = -1;
    srand(time(NULL));

    wind += 2 * random(0,1);

    CCLOG("wiater %d",wind);

    scheduleUpdate();

    return true;
}

void CloudSpawner::update(float dt)
{
    timer += dt;

    if (timer > deltaTime)
    {
        timer = 0;
        auto visibleSize = Director::getInstance()->getVisibleSize();
       
        int tempY = rand() % (int)(visibleSize.height / 3);
        int y = visibleSize.height / 2 + tempY;
        int x;

        auto c = Cloud::create();

        Size cloudSize = c->getContentSize() / 2 * visibleSize.width / 1080;

        if (wind = -1)
        {
            x = visibleSize.width + cloudSize.width;
            CCLOG("cloud %d", x);
        }
        else
        {
            x = -cloudSize.width;
        }

        c->setPosition(Vec2(x,y));
        this->getParent()->addChild(c, -98);
        c->setSpeed(wind, tempY);
    }
}