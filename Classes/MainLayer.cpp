#include "MainLayer.h"
#include "Fish\Fish.h"
#include "Weapons\TriangularGun.h"
#include "Fish\Spawner.h"
#include "CloudSpawner.h"

USING_NS_CC;

bool MainLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();

    triGun = TriangularGun::create();
    triGun->setPosition(0, 0);
		triGun->setLayer(this);
		this->addChild(triGun, 1000);
		
    auto cloudSpawner = CloudSpawner::create();
    this->addChild(cloudSpawner);

    fishSpawner = Spawner::create();
    fishSpawner->setPosition(0, 0);
    this->addChild(fishSpawner, 2);

    auto bckg = LayerColor::create(Color4B(103, 192, 234, 255.0));
    bckg->setPosition(Point(0,0));
    this->addChild(bckg, -99);

    auto waterShader = GLProgram::createWithFilenames("water.vsh", "water.fsh");
    waterShader->retain();
    waterShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    waterShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    waterShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
    waterShader->link();
    waterShader->updateUniforms();
    waterShader->use();


    auto waterSolidShader = GLProgram::createWithFilenames("water.vsh", "waterSolid.fsh");
    waterSolidShader->retain();
    waterSolidShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    waterSolidShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    waterSolidShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
    waterSolidShader->link();
    waterSolidShader->updateUniforms();
    waterSolidShader->use();

    auto water = Sprite::create("water.png");
    water->setPosition(visibleSize / 2);
    water->setAnchorPoint(Point(0.5, 0.5));
    water->setScale(visibleSize.width / 1080);
    this->addChild(water, 99);

    auto waterShaderState = GLProgramState::getOrCreateWithGLProgram(waterShader);
    waterShaderState->setUniformFloat("u_size", visibleSize.width);

    auto waterSolid = Sprite::create("water.png");
    waterSolid->setPosition(visibleSize / 2);
    waterSolid->setAnchorPoint(Point(0.5, 0.5));
    waterSolid->setScale(visibleSize.width / 1080);
    this->addChild(waterSolid, 0);

    auto waterSolidShaderState = GLProgramState::getOrCreateWithGLProgram(waterSolidShader);
    waterSolidShaderState->setUniformFloat("u_size", visibleSize.width);

    waterSolid->setGLProgram(waterSolidShader);
    water->setGLProgram(waterShader);
    
    scheduleUpdate();

    return true;
}


void MainLayer::update(float dt)
{
}

void MainLayer::addDestroyable(Destroyable * d)
{
    destroyables.pushBack(d);
}

void MainLayer::removeDestroyable(Destroyable * d)
{
    destroyables.eraseObject(d);
}

void MainLayer::clearDestroyables()
{
	for (auto d : destroyables)
	{
		if (!d->burn())
		{
			return;
		}
	}
}

Vector<Destroyable *> MainLayer::getDestroyables()
{
    return destroyables;
}

Spawner * MainLayer::getFishSpawner()
{
	return fishSpawner;
}