#pragma once

#include "cocos2d.h"

class Triangle : public cocos2d::Node
{
public:
    Triangle();
    Triangle(cocos2d::Vec2 p1, cocos2d::Vec2 p2, cocos2d::Vec2 p3);
    float getArea();
    bool intersectLine(cocos2d::Vec2 A, cocos2d::Vec2 B);
    bool isPointInside(cocos2d::Vec2 point);
    bool intersectRect(cocos2d::Rect r);
    void draw(cocos2d::Renderer* renderer, const cocos2d::kmMat4& transform, uint32_t flags);
    void onDraw(const cocos2d::Mat4 &transform, uint32_t flags);
    cocos2d::Rect intersectionRect(cocos2d::Rect r);
    cocos2d::Rect * intersectionRect(cocos2d::Vec2 A, cocos2d::Vec2 B);
    cocos2d::Rect getBoundingBox();
    cocos2d::Vec2 * points[3];
private:    
    cocos2d::CustomCommand _customCommand;
    cocos2d::Rect boundingBox;
};

