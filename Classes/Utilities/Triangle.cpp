#include "Triangle.h"

USING_NS_CC;

Triangle::Triangle()
{
    points[0] = nullptr;
    points[1] = nullptr;
    points[2] = nullptr;
}

Triangle::Triangle(Vec2 p1, Vec2 p2, Vec2 p3)
{
    points[0] = new Vec2(p1);
    points[1] = new Vec2(p2);
    points[2] = new Vec2(p3);

    int x = MIN(points[0]->x, MIN(points[1]->x, points[2]->x));
    int y = MIN(points[0]->y, MIN(points[1]->y, points[2]->y));
    int width = MAX(points[0]->x, MAX(points[1]->x, points[2]->x)) - x;
    int height = MAX(points[0]->y, MAX(points[1]->y, points[2]->y)) - y;

    boundingBox = CCRectMake(x, y, width, height);

}

void Triangle::draw(Renderer* renderer, const kmMat4& transform, uint32_t flags)
{
    _customCommand.init(_globalZOrder);
    _customCommand.func = CC_CALLBACK_0(Triangle::onDraw, this, transform, flags);
    renderer->addCommand(&_customCommand);
}

void Triangle::onDraw(const Mat4 &transform, uint32_t flags)
{
    kmGLPushMatrix();
    kmGLLoadMatrix(&transform);
    Point p[] = {
        Point(*points[0]),
        Point(*points[1]),
        Point(*points[2])
    };
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    ccDrawColor4F(0.0f, 0.0f, 1.0f, 1.0f);
    ccDrawLine(*points[0], *points[1]);
    ccDrawLine(*points[0], *points[2]);
    ccDrawLine(*points[1], *points[2]);
//    DrawPrimitives::drawSolidPoly(p, 3, Color4F(0.0f, 0.0f, 1.0f, 1.0f));
   
    kmGLPopMatrix();
}

float Triangle::getArea()
{
    float a = points[0]->distance(*points[1]);
    float b = points[0]->distance(*points[2]);
    float c = points[1]->distance(*points[2]);

    float s = (a + b + c) / 2;

    return sqrt(s*(s - a)*(s - b)*(s - c));
}

bool Triangle::isPointInside(Vec2 point)
{
    float area = 0.5*(-points[1]->y * points[2]->x + points[0]->y * ( - points[1]->x + points[2]->x) + 
                                                 points[0]->x * (   points[1]->y - points[2]->y) + points[1]->x * points[2]->y);
    
    float s = 1 / (2 * area) * (points[0]->y * points[2]->x - points[0]->x * points[2]->y + (points[2]->y - points[0]->y) * point.x + (points[0]->x - points[2]->x) * point.y);
    float t = 1 / (2 * area) * (points[0]->x * points[1]->y - points[0]->y * points[1]->x + (points[0]->y - points[1]->y) * point.x + (points[1]->x - points[0]->x) * point.y);
    return (s>0 && t>0 && 1-s-t>0);
}

Rect Triangle::getBoundingBox()
{
    return boundingBox;
}

bool Triangle::intersectRect(Rect r)
{
    if (boundingBox.intersectsRect(r) && 
       (this->intersectLine(Vec2(r.getMinX(), r.getMinY()), Vec2(r.getMinX(), r.getMaxY())) ||
        this->intersectLine(Vec2(r.getMinX(), r.getMinY()), Vec2(r.getMaxX(), r.getMinY())) || 
        this->intersectLine(Vec2(r.getMaxX(), r.getMaxY()), Vec2(r.getMinX(), r.getMaxY())) || 
        this->intersectLine(Vec2(r.getMaxX(), r.getMaxY()), Vec2(r.getMaxX(), r.getMinY())) ))
    {
        return true;
    }
    return false;
}

bool Triangle::intersectLine(Vec2 A, Vec2 B)
{
    if (Vec2::isSegmentIntersect(A, B, *points[0], *points[1]) ||
        Vec2::isSegmentIntersect(A, B, *points[0], *points[2]) ||
        Vec2::isSegmentIntersect(A, B, *points[1], *points[2]))
    {
        return true;
    }
    
    return false;
}

Rect Triangle::intersectionRect(Rect r)
{
    float minX = -66.6f;
    float minY = -66.6f;
    float maxX = -66.6f;
    float maxY = -66.6f;

    Vec2 v0 = Vec2(r.getMinX(), r.getMinY());
    Vec2 v1 = Vec2(r.getMaxX(), r.getMinY());
    Vec2 v2 = Vec2(r.getMaxX(), r.getMaxY());
    Vec2 v3 = Vec2(r.getMinX(), r.getMaxY());

    Rect rect = Rect::ZERO;

    Rect * r0 = intersectionRect(v0, v1);
    Rect * r1 = intersectionRect(v0, v3);
    Rect * r2 = intersectionRect(v2, v1);
    Rect * r3 = intersectionRect(v2, v3);

    /*
    MAGIC
    */

    if (r0 != nullptr)
    {
        rect = *r0;
    }

    if (r1 != nullptr)
    {
        if (rect.equals(Rect::ZERO))
        {
            rect = *r1;
        }
        else
        {
            rect = rect.unionWithRect(*r1);
        }
    }

    if (r2 != nullptr)
    {
        if (rect.equals(Rect::ZERO))
        {
            rect = *r2;
        }
        else
        {
            rect = rect.unionWithRect(*r2);
        }
    }

    if (r3 != nullptr)
    {
        if (rect.equals(Rect::ZERO))
        {
            rect = *r3;
        }
        else
        {
            rect = rect.unionWithRect(*r3);
        }
    }

    /*MORE MAGIC*/

    if (r.containsPoint(*points[0]))
    {
        Rect pointRect = Rect(points[0]->x, points[0]->y, 1, 1);
        rect = rect.unionWithRect(pointRect);
    }
    if (r.containsPoint(*points[1]))
    {
        Rect pointRect = Rect(points[1]->x, points[1]->y, 1, 1);
        rect = rect.unionWithRect(pointRect);
    }
    if (r.containsPoint(*points[2]))
    {
        Rect pointRect = Rect(points[2]->x, points[2]->y, 1, 1);
        rect = rect.unionWithRect(pointRect);
    }

    return rect;
}

Rect * Triangle::intersectionRect(Vec2 A, Vec2 B)
{
    float minX = -66.6f;
    float minY = -66.6f;
    float maxX = -66.6f;
    float maxY = -66.6f;
    
    /*MAGIC AGAIN*/
    if (Vec2::isSegmentIntersect(A, B, *points[0], *points[1]))
    {
        Vec2 v = Vec2::getIntersectPoint(A, B, *points[0], *points[1]); 

        if (minX == -66.6f)
        {
            minX = maxX = v.x;
            minY = maxY = v.y;
        }
        else
        {
            if (v.x < minX)
            {
                minX = v.x;
            }
            else if (v.x > maxX)
            {
                maxX = v.x;
            }
            if (v.y < minY)
            {
                minY = v.y;
            }
            else if (v.y > maxY)
            {
                maxY = v.y;
            }
        }
    }

    if (Vec2::isSegmentIntersect(A, B, *points[0], *points[2]))
    {
        Vec2 v = Vec2::getIntersectPoint(A, B, *points[0], *points[2]);

        if (minX == -66.6f)
        {
            minX = maxX = v.x;
            minY = maxY = v.y;
        }
        else
        {
            if (v.x < minX)
            {
                minX = v.x;
            }
            else if (v.x > maxX)
            {
                maxX = v.x;
            }
            if (v.y < minY)
            {
                minY = v.y;
            }
            else if (v.y > maxY)
            {
                maxY = v.y;
            }
        }
    }

    if (Vec2::isSegmentIntersect(A, B, *points[1], *points[2]))
    {
        Vec2 v = Vec2::getIntersectPoint(A, B, *points[1], *points[2]);

        if (minX == -66.6f)
        {
            minX = maxX = v.x;
            minY = maxY = v.y;
        }
        else
        {
            if (v.x < minX)
            {
                minX = v.x;
            }
            else if (v.x > maxX)
            {
                maxX = v.x;
            }
            if (v.y < minY)
            {
                minY = v.y;
            }
            else if (v.y > maxY)
            {
                maxY = v.y;
            }
        }
    }

    if (minX != -66.6f)
    {
        return new Rect(minX, minY, maxX - minX, maxY - minY);
    }

    return nullptr;
}
