#pragma once

#include "cocos2d.h"

class Explosion : public cocos2d::Sprite
{
public:
    bool init();
    void update(float);
    void setup(cocos2d::Vec2 position);
    CREATE_FUNC(Explosion);    
    cocos2d::ScaleTo * explode;
private:
    float time = 0.2f;
};

