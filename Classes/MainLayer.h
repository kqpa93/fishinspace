#pragma once

#include "cocos2d.h"
#include "Destroyable.h"
#include "Weapons\TriangularGun.h"
#include "Fish\Spawner.h"

class MainLayer : public cocos2d::Layer
{
public:
    virtual bool init();
    void update(float);
    CREATE_FUNC(MainLayer);
    TriangularGun* triGun;
		bool runing{ false };
    cocos2d::Vector<Destroyable *> getDestroyables();
    void addDestroyable(Destroyable * d);
    void removeDestroyable(Destroyable * d);
		void clearDestroyables();
		Spawner * getFishSpawner();
private:
    cocos2d::Vector<Destroyable *> destroyables;
		Spawner * fishSpawner;
};