#include "Plane.h"
#include "MainLayer.h"
#define PI 3.14

USING_NS_CC;

bool Plane::init()
{
    if (!initWithFile("plane.png"))
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();

    int dir = rand() % 2;

    float y = 0.6 * visibleSize.height * 0.05;
    int r = random(0, 20);
    
    setPosition(-100 + (visibleSize.width + 200)* dir, visibleSize.height * 0.3 + y*r);

    float scale = (visibleSize.width / 1080.0);

    speed = (200.0f + rand() % 10) * scale;

    direction = Vec2(1 - 2 * dir, 0);
    setScaleY(scale);
    setScaleX((1 - 2 * dir ) * scale);

    direction = direction.rotateByAngle(Vec2(0, 0), -PI/24 + (rand() % 1000)*PI/12000.0);

    scheduleUpdate();

    return true;
}

void Plane::update(float dt)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto layer = dynamic_cast<MainLayer*>(getParent());

    setPosition(this->getPosition() + direction*speed*dt);
    if (getPosition().x - 300 > visibleSize.width ||
        getPosition().x + 300 < 0)
    {
        layer->removeDestroyable(this);
        layer->removeChild(this);
    }
}
