#include "Satelite.h"
#include "MainLayer.h"

#define PI 3.14

USING_NS_CC;

bool Satelite::init()
{
    if (!initWithFile("Satelite.png"))
    {
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();



    speed = (400.0f + rand() % 10) * visibleSize.width / 1080;

    int dir = rand() % 2;

    setPosition(-100 + (visibleSize.width + 200)* dir, visibleSize.height * 0.5 + rand() % (int)(visibleSize.height / 4));

    direction = Vec2(1 - 2 * dir, 0);
    setScaleY( visibleSize.width / 1080);
    setScaleX((1 - 2 * dir) * visibleSize.width / 1080);

    direction = direction.rotateByAngle(Vec2(0, 0), -PI / 24 + (rand() % 1000)*PI / 12000.0);

    scheduleUpdate();

    return true;
}

void Satelite::update(float dt)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto layer = dynamic_cast<MainLayer*>(getParent());


    setPosition(this->getPosition() + direction*speed*dt);
    if (getPosition().x - 300 > visibleSize.width ||
        getPosition().x + 300 < 0)
    {
        layer->removeDestroyable(this);
        layer->removeChild(this);
    }
}
