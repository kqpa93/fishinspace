#include "Rocket.h"
#include "MainLayer.h"

#define PI 3.14
#define MATH_RAD_TO_DEG 180/PI

USING_NS_CC;

bool Rocket::init()
{
    if (!initWithFile("rocket.png"))
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();



    speed = (350.0f + rand() % 30) * visibleSize.width / 1080;


    setPosition(50 + rand()%(int)(visibleSize.width -100), -300);

    direction = Vec2(0, 1);
    setScale(visibleSize.width / 1080);

    int dir = random(0, 1);

    if (dir == 0) --dir;

    float angle = (-dir)*(rand() % 1000) / 1000.0 * PI / 6;

    direction = direction.rotateByAngle(Vec2(0, 0), angle);
    this->setRotation(-angle*MATH_RAD_TO_DEG);

    scheduleUpdate();
    return true;
}

void Rocket::update(float dt)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto layer = dynamic_cast<MainLayer*>(getParent());

    setPosition(this->getPosition() + direction*speed*dt);
    if (getPosition().y - 300 > visibleSize.height)
    {
        layer->removeDestroyable(this);
        layer->removeChild(this);
    }
}
