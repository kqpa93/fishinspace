#pragma once

#include "cocos2d.h"
#include "Shuttle.h"

class Submarine : public Shuttle
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(Submarine);

private:
    cocos2d::Vec2 direction;
    float speed;
};

