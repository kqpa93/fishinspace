#pragma once

#include "cocos2d.h"
#include "Shuttle.h"

class Plane : public Shuttle
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(Plane);

private:
    cocos2d::Vec2 direction;
    float speed;
};

