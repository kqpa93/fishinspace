#pragma once

#include "cocos2d.h"
#include "Shuttle.h"

class Rocket : public Shuttle
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(Rocket);
private:

    cocos2d::Vec2 direction;
    float speed;

};

