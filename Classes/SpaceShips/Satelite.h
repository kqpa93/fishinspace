#pragma once

#include "cocos2d.h"
#include "Shuttle.h"

class Satelite : public Shuttle
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(Satelite);
private:

    cocos2d::Vec2 direction;
    float speed;

};

