#include "Explosion.h"
#include "MainLayer.h"

USING_NS_CC;

bool Explosion::init()
{
    if (!initWithFile("Effects/Bang.png"))
    {
        return false;
    }


    setScale(0);
   
    return true;
}

void Explosion::setup(Vec2 position)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();


    setPosition(position);
   
    explode = ScaleTo::create(time, 1.0 * visibleSize.width / 1080.0);
    explode->setTag(0);
    this->runAction(explode);

    scheduleUpdate();
}

void Explosion::update(float dt)
{
    if (time <= 0.0f)
    {
        auto layer = dynamic_cast<MainLayer *>(getParent());
        for (auto &child : layer->getDestroyables())
        {
            child->burn();
        }
        layer->removeChild(this);
    }
    else
    {
        time -= dt;
    }
}
