#include "Cloud.h"
#define SPEED 100

USING_NS_CC;

bool Cloud::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    float scale = visibleSize.width / 1080.0;

    int r = random(1,3);

    std::string fileName = String::createWithFormat("Clouds/Cloud%d.png", r)->getCString();

    if (!initWithFile(fileName))
    {
        return false;
    }
    
    setAnchorPoint(Point(0.5, 0.5));
    setScale(scale);

    auto shadowShader = GLProgram::createWithFilenames("gray.vsh", "gray.fsh");
    shadowShader->retain();
    shadowShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    shadowShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    shadowShader->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
    shadowShader->link();
    shadowShader->updateUniforms();
    shadowShader->use();
    
    auto shadow = Sprite::create(fileName);
    shadow->setAnchorPoint(Point(-0.1, 0.1));
    //shadow->setPosition(Vec2(10, -10) * scale);
    shadow->setGLProgram(shadowShader);
    this->addChild(shadow, -1);

    speed = 0;


    scheduleUpdate();

    return true;
}

void Cloud::setSpeed(int w, int y)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    float scale = 1.0f - 0.5f*(3*y)/(visibleSize.height);

    speed = SPEED * scale * w * visibleSize.width / 1080.0;
    CCLOG("%d", speed);


    setScale(getScale() * scale);
}


void Cloud::update(float dt)
{
    this->setPosition(this->getPosition() + Vec2(speed, 0)*dt);
}
