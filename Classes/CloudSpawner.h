#pragma once

#include "cocos2d.h"


class CloudSpawner : public cocos2d::Node
{
public:
    bool init();
    void update(float);
    CREATE_FUNC(CloudSpawner);
private:
    float deltaTime;
    float timer;
    int wind;
};