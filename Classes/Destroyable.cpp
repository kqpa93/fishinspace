#include "Destroyable.h"
#include "Explosion.h"
#include "MainLayer.h"

bool Destroyable::burn()
{
    auto explosion = Explosion::create();
    getParent()->addChild(explosion);
    explosion->setup(getPosition());
    
    auto layer = dynamic_cast<MainLayer *>(getParent());
    
    layer->removeDestroyable(this);
    layer->removeChild(this);
    
    return false;
}

int Destroyable::getPoints()
{
    return points;
}

bool Destroyable::isAlive()
{
    return alive;
}