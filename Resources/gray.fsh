
varying vec4 v_fragmentColor;	
varying vec2 v_texCoord;	
		
void main()			
{
	vec4 v_orColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
	if(v_orColor.r > 0.0)
	{
		v_orColor = vec4(vec3(0.2), 0.7);
	}
	//v_orColor.a = 0.3;
	gl_FragColor = v_orColor;
}				
