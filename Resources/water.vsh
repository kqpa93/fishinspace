attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
				
uniform float u_size;

varying vec4 v_position;
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
varying float s;

void main()	
{
	s = u_size;
	v_position = CC_PMatrix * a_position;
	gl_Position = v_position;
	v_fragmentColor = a_color;
	v_texCoord = a_texCoord;
}