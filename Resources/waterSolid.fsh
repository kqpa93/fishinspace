varying vec4 v_position;
varying vec4 v_fragmentColor;	
varying vec2 v_texCoord;	
		
varying float s;

void main()			
{
	float size = s;

	vec4 waterBlue = vec4(50.0/255.0, 75.0/255.0, 133.0/255.0, 1.0);

	float waterWidth = 0.0;

	float x = 0.003 * v_position.x;
	x += size * 0.002 * cos(CC_Time.x * 2.0 + 0.001 * v_position.x);
	x += size * 0.001 * cos((CC_Time.x-1.0) * 2.0 + 0.002 * v_position.x);
	x += size * 0.0005 * cos((CC_Time.x-2.0) * 2.0 + 0.004 * v_position.x);

	int i = 1;

	waterWidth += size * 0.05 * sin(CC_Time.x * 2.0 + x );
	waterWidth += size * 0.01 * sin((CC_Time.x - 1.0) * 2.0 + x );
	waterWidth += size * 0.005 * sin((CC_Time.x - 2.0) * 2.0 + x );

	float waterLevel = -size + waterWidth;

	 	gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);

	if (v_position.y < waterLevel + 3.0)
	{
		gl_FragColor = waterBlue;
	}
	else
	{
	 	gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
	}	
}		
