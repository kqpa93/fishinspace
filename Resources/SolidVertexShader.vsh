attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

varying vec2 v_texCoord;
varying vec4 v_color;
varying vec4 v_position;

void main() {
    v_color = a_color;
    v_position = CC_MVPMatrix * a_position;
    gl_Position = v_position;
    v_texCoord = a_texCoord;
}