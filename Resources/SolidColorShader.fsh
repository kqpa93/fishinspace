varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
varying vec4 v_position;

void main()            
{
  	vec4 color = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
  	if(color.a> 0.0){
    	gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	}
	else{
	  	gl_FragColor = vec4(color.a, 0.0, 0.0, 0.0);
	}
}