LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/Scenes/MainScene.cpp \
                   ../../Classes/PerPixelCollision/CollisionDetection.cpp \
                   ../../Classes/PerPixelCollision/PixelsReadNode.cpp \
                   ../../Classes/Fish/CookedFish.cpp \
                   ../../Classes/Fish/Fish.cpp \
                   ../../Classes/Fish/Spawner.cpp \
                   ../../Classes/Fish/SuperFish.cpp \
                   ../../Classes/Fish/SmallFish.cpp \
                   ../../Classes/SpaceShips/Shuttle.cpp \
                   ../../Classes/SpaceShips/SpaceStation.cpp \
                   ../../Classes/SpaceShips/Plane.cpp \
                   ../../Classes/SpaceShips/Rocket.cpp \
                   ../../Classes/SpaceShips/Satelite.cpp \
                   ../../Classes/SpaceShips/Submarine.cpp \
                   ../../Classes/Weapons/TriangularGun.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/Cloud.cpp \
                   ../../Classes/CloudSpawner.cpp \
                   ../../Classes/Explosion.cpp \
                   ../../Classes/Destroyable.cpp \
                   ../../Classes/MainLayer.cpp \
                   ../../Classes/Triangle.cpp 




LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_STATIC_LIBRARIES := cocos2dx_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
